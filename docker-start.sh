#!/bin/bash

# install yarn if it's not already installed
install_yarn() {
  if ! hash yarn 2>/dev/null; then
    npm install -g yarn
  fi
}

install_yarn

yarn

yarn run dev-start
