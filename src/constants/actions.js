export const SIGNUP_LOGIN_SUCCESS = 'SIGNUP_LOGIN_SUCCESS';
export const LOGOUT_SUCCESS = 'LOGOUT_SUCCESS';
export const NEW_EMAIL_CONFIRM_SUCCESS = 'NEW_EMAIL_CONFIRM_SUCCESS';
export const EMAIL_CONFIRM_SUCCESS = 'EMAIL_CONFIRM_SUCCESS';
export const EMAIL_CONFIRM_FAILED = 'EMAIL_CONFIRM_FAILED';

export const PROFILE_UPDATE_SUCCESS = 'PROFILE_UPDATE_SUCCESS';

export const QUERY_MAP = 'QUERY_MAP';

export const GET_LAMPS = 'GET_LAMPS';
export const SINGLE_LAMP_FETCH_SUCCESS = 'SINGLE_LAMP_FETCH_SUCCESS';

export const GET_REGIMES = 'GET_REGIMES';

export const NOTIFICATIONS_FETCH_SUCCESS = 'NOTIFICATIONS_FETCH_SUCCESS';
export const NOTIFICATIONS_LAMPS_FETCH_SUCCESS =
  'NOTIFICATIONS_LAMPS_FETCH_SUCCESS';

export const GROUPS_FETCH_SUCCESS = 'GROUPS_FETCH_SUCCESS';
export const GROUP_REGIME_CHANGE_SUCESS = 'GROUP_REGIME_CHANGE_SUCESS';
export const GROUP_REGIME_CHANGE_FAIL = 'GROUP_REGIME_CHANGE_FAIL';
export const GROUP_LAMPS_FETCH_SUCCESS = 'GROUP_LAMPS_FETCH_SUCCESS';
export const GROUP_LAMPS_FETCH_FAIL = 'GROUP_LAMPS_FETCH_FAIL';

export const LAMPS_PAGINATION_CHANGED = 'LAMPS_PAGINATION_CHANGED';
export const USERS_PAGINATION_CHANGED = 'USERS_PAGINATION_CHANGED';
export const GROUPS_PAGINATION_CHANGED = 'GROUPS_PAGINATION_CHANGED';

export const USER_ZONES_FETCH_SUCCESS = 'USER_ZONE_FETCH_SUCCESS';
export const ZONE_USERS_FETCH_SUCCESS = 'ZONE_USERS_FETCH_SUCCESS';
export const USER_CURRENT_ZONE_CHANGE_SUCCESS =
  'USER_CURRENT_ZONE_CHANGE_SUCCESS';
