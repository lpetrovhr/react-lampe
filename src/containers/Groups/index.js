import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router';
import { Grid, Row, Col, Table, Clearfix } from 'react-bootstrap';
import { groupsGetFetch, groupsSetRegime } from '../../actions/groups';
import { fetchRegimes } from '../../actions/regimes';

export class GroupsComponent extends Component {
  static propTypes = {
    dispatch: PropTypes.func.isRequired,
    groups: PropTypes.object.isRequired,
    zones: PropTypes.object.isRequired,
    regimes: PropTypes.object.isRequired,
  };

  componentWillMount() {
    const { zones } = this.props;

    const zoneId = zones.getIn(['currentZone', 'id']);

    if (zoneId) {
      this.fetchData(zoneId);
    }
  }

  componentWillReceiveProps(nextProps) {
    const nextZoneId = nextProps.zones.getIn(['currentZone', 'id']);
    const currentZoneId = this.props.zones.getIn(['currentZone', 'id']);
    if (currentZoneId !== nextZoneId) {
      this.fetchData(nextZoneId);
    }
  }

  onRegimeSelectChange = (e, groupId) => {
    const { dispatch, zones } = this.props;
    const { currentZone } = zones.toJS();
    dispatch(groupsSetRegime(e.target.value, groupId, currentZone.id));
  }

  fetchData(zoneId) {
    const { dispatch } = this.props;

    dispatch(groupsGetFetch(zoneId));
    dispatch(fetchRegimes(zoneId));
  }

  renderRegimeOptions = (groupId) => {
    const { regimes } = this.props;
    const { regimesList } = regimes.toJS();

    return regimesList && regimesList.map(regime =>
      <option value={regime.id} key={regime.id} id={groupId}>
        {regime.name}
      </option>
    );
  };

  renderGroupsTable = (groupsList, currentZone) =>
    groupsList && groupsList.map(group =>
      <tr className="tableDataAlign" key={group.id}>
        <td>{group.id}</td>
        <td>{group.name}</td>
        <td>{group.zone.name}</td>
        <td>{currentZone.userZone.role === 'admin' ?
          <select
            id="regimeSelect"
            className="form-control input-sm"
            onChange={e => this.onRegimeSelectChange(e, group.id)}
            defaultValue={group.regime.id}
            key={group.id}
          >
            {this.renderRegimeOptions()}
          </select> : group.regime.name }
        </td>
        <td>
          <Link to={`/groups/${group.id}/lamps`}>
            <button className="btn btn-default btn-xs">Lista svjetiljki</button>
          </Link>
        </td>
      </tr>
    );

  render() {
    const { groups, zones } = this.props;
    const { groupsList, count } = groups.toJS();
    const { currentZone } = zones.toJS();

    return (
      <Grid>
        <div className="title_left"><h3>Grupe</h3></div>
        <div className="x_panel">
          <Row className="row">
            <Col md={12} sm={12} xs={12}>
              <div className="x_title">
                <h2>Popis grupa</h2>
                <Clearfix />
              </div>
              <div className="x_content">
                {count > 0 ?
                  <Table bordered striped condensed hover responsive>
                    <thead>
                      <tr>
                        <th>ID</th>
                        <th>Ime</th>
                        <th>Zona</th>
                        <th>Režim</th>
                        <th>Akcije</th>
                      </tr>
                    </thead>
                    <tbody>
                      {this.renderGroupsTable(groupsList, currentZone)}
                    </tbody>
                  </Table> : <p>Trenutno nema dostupnih grupa</p>
                }
              </div>
            </Col>
          </Row>
        </div>
      </Grid>
    );
  }
}

export default connect(state => ({
  user: state.get('user'),
  groups: state.get('groups'),
  zones: state.get('zones'),
  regimes: state.get('regimes'),
}))(GroupsComponent);
