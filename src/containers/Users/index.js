import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import { Grid, Row, Col, Table, Clearfix } from 'react-bootstrap';
import { fetchZoneUsers } from '../../actions/zones';
import { LANGUAGES as languages } from '../../constants/application';

export class UsersComponent extends Component {
  static propTypes = {
    dispatch: PropTypes.func.isRequired,
    zones: PropTypes.object.isRequired,
    router: PropTypes.object.isRequired,
  };

  componentWillMount() {
    const { zones } = this.props;

    const zoneId = zones.getIn(['currentZone', 'id']);

    if (zoneId) {
      this.getZoneUsers(zoneId);
    }
  }

  componentWillReceiveProps(nextProps) {
    const nextZoneId = nextProps.zones.getIn(['currentZone', 'id']);
    const currentZoneId = this.props.zones.getIn(['currentZone', 'id']);
    if (currentZoneId !== nextZoneId) {
      this.getZoneUsers(nextZoneId);
    }
  }

  getZoneUsers = (zoneId) => {
    const { dispatch } = this.props;
    dispatch(fetchZoneUsers(zoneId)).then(() =>
      this.zoneChanged()
    );
  };

  zoneChanged = () => {
    const { zones, router } = this.props;
    const { currentZone } = zones.toJS();
    if (currentZone.userZone.role !== 'admin') {
      router.push('/maps');
    }
  };

  paginationPageChanged = (page) => {
    this.getPaginatedList(page);
  };

  rolesTranslate = (role, language) => {
    let rolePlace = 0;
    switch (role) {
      case 'admin': {
        rolePlace = 2;
        break;
      }
      case 'editor': {
        rolePlace = 1;
        break;
      }
      case 'viewer': {
        rolePlace = 0;
        break;
      }
      default:
        rolePlace = 0;
    }

    const roleLang = languages[language].roles[rolePlace];

    return roleLang;
  };

  renderTableData = usersList =>
    usersList && usersList.map(user =>
      <tr key={user.id}>
        <td>{user.firstname} {user.lastname}</td>
        <td>{this.rolesTranslate(user.userZone.role, 'hr')}</td>
      </tr>,
    );

  render() {
    const { zones } = this.props;
    const { usersList } = zones.toJS();

    return (
      <Grid>
        <div className="title_left"><h3>Korisnici</h3></div>
        <div className="x_panel">
          <Row>
            <Col md={12} sm={12} xs={12}>
              <div className="x_title">
                <h2>Lista korisnika i njihovih prava</h2>
                <Clearfix />
              </div>
              <div className="x_content">
                <Table bordered striped condensed hover responsive>
                  <thead>
                    <tr>
                      <th>Ime i prezime</th>
                      <th>Prava</th>
                    </tr>
                  </thead>
                  <tbody>
                    {this.renderTableData(usersList)}
                  </tbody>
                </Table>
              </div>
            </Col>
          </Row>
        </div>
      </Grid>
    );
  }
}

export default connect(state => ({
  user: state.get('user'),
  zones: state.get('zones'),
}))(withRouter((UsersComponent)));
