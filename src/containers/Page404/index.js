import React from 'react';
import { IndexLink, Link } from 'react-router';
import { Grid, Row, Col } from 'react-bootstrap';

const Page404 = () =>
  <Grid>
    <Row>
      <Col md={12} sm={12} xs={12}>
        <div className="col-middle">
          <div className="text-center text-center">
            <h1 className="error-number">404</h1>
            <h2>Ova stranica ne postoji</h2>
            <p>
              Vratite se na <IndexLink to="/">Pocetnu</IndexLink> stranicu
              ili se <Link to="/login">Prijavite</Link>.
            </p>
          </div>
        </div>
      </Col>
    </Row>
  </Grid>;

export default Page404;
