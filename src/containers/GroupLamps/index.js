import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { Grid, Row, Col, Table } from 'react-bootstrap';
import { groupLampsFetch, paginationPageChanged } from '../../actions/lamps';
import LampStatus from '../../components/LampStatus';
import Pagination from '../../components/Pagination';
import { ITEMS_PER_PAGE, STATUS_ALL,
  STATUS_ONLINE } from '../../constants/application';

export class GroupLampsComponent extends Component {
  static propTypes = {
    dispatch: PropTypes.func.isRequired,
    lamps: PropTypes.object,
    params: PropTypes.object,
    zones: PropTypes.object.isRequired,
  };

  constructor() {
    super();
    this.getLamps = this.getLamps.bind(this);
    this.statusChange = this.statusChange.bind(this);
    this.paginationPageChanged = this.paginationPageChanged.bind(this);
    this.getPaginatedList = this.getPaginatedList.bind(this);
    this.onPaginationPageChanged = this.onPaginationPageChanged.bind(this);
  }

  state = {
    lampStatus: STATUS_ALL,
  }

  componentWillMount() {
    this.paginationPageChanged(1);
  }

  onPaginationPageChanged = (page) => {
    const { dispatch } = this.props;

    dispatch(paginationPageChanged(page));
  };

  getLamps = (state, page, cb) => {
    const { dispatch, params, zones } = this.props;
    const groupId = params.groupId;
    const { currentZone } = zones.toJS();

    dispatch(groupLampsFetch(currentZone.id, groupId, page, state, cb));
  };

  getPaginatedList = (page) => {
    this.getLamps(this.state.lampStatus, page, () => {
      this.onPaginationPageChanged(page);
    });
  };

  paginationPageChanged = (page) => {
    this.getPaginatedList(page);
  };

  statusChange = (event) => {
    this.setState({ lampStatus: event.target.value });

    this.getLamps(event.target.value, 1);
  };

  renderTableData = () => {
    const { lamps } = this.props;
    const { lampsList } = lamps.toJS();
    const lampsListRow = lampsList.rows;

    const working = {
      color: '#005900',
    };

    const notWorking = {
      color: '#B20000',
    };

    if (typeof lampsListRow === 'undefined') { return null; }

    return lampsListRow.map(marker =>
      <tr className="tableDataAlign" key={marker.id}>
        <td>{marker.id}</td>
        <td>{marker.name}</td>
        <td>
          <span
            style={marker.status !== STATUS_ONLINE ? notWorking : working}
            className="fa fa-circle"
          />
        </td>
        <td>
          {marker.lampCoord.lat} - {marker.lampCoord.lng}
        </td>
      </tr>
    );
  };

  render() {
    const { lamps, zones } = this.props;
    const { currentPage, lampsList } = lamps.toJS();

    return (
      <Grid>
        <div className="title_left">
          <h3>Grupa {zones.getIn(['currentZone', 'name'])} - Svjetiljke</h3>
        </div>
        <div className="x_panel">
          <Row className="row">
            <div className="x_title">
              <LampStatus
                lampStatus={this.state.lampStatus}
                statusChange={this.statusChange}
              />
            </div>
            <Col md={12} sm={12} xs={12}>
              {lampsList.count > 0 ?
                <Table bordered striped condensed hover responsive>
                  <thead>
                    <tr>
                      <th>ID</th>
                      <th>Ime</th>
                      <th>Status</th>
                      <th>Geografska širina - dužina</th>
                    </tr>
                  </thead>
                  <tbody>
                    {this.renderTableData()}
                  </tbody>
                </Table> : <p>Ova grupa trenutno ne sadrži lampe.</p>}
            </Col>
          </Row>
          <Pagination
            currentPage={currentPage}
            itemsPerPage={ITEMS_PER_PAGE}
            total={lampsList.count}
            onPaginationChange={this.paginationPageChanged}
          />
        </div>
      </Grid>
    );
  }
}

export default connect(state => ({
  user: state.get('user'),
  lamps: state.get('lamps'),
  zones: state.get('zones'),
}))(GroupLampsComponent);
