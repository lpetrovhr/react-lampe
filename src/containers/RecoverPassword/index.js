import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { Field, reduxForm } from 'redux-form/immutable';
import { withRouter } from 'react-router';
import { Form, FormGroup, Button } from 'react-bootstrap';
import { recoverPasswordFetch } from '../../actions/auth';
import { isPassword, isRequired, isSamePassword } from '../../utils/validator';
import { REDIRECTION } from '../../constants/application';
import Input from '../../components/Input';
import ErrorMsg from '../../components/ErrorMsg';
import SuccessMsg from '../../components/SuccessMsg';

export const validate = (values) => {
  const errors = {};
  const { password, confirmation } = values.toJS();

  errors.password = isRequired(password, 'hr') || isPassword(password, 'hr');
  errors.confirmation = isRequired(confirmation, 'hr') ||
    isPassword(confirmation, 'hr') ||
    isSamePassword(confirmation, password, 'hr');
  return errors;
};

export class RecoverPasswordComponent extends Component {
  static propTypes = {
    dispatch: PropTypes.func.isRequired,
    error: PropTypes.string,
    handleSubmit: PropTypes.func.isRequired,
    params: PropTypes.object.isRequired,
    router: PropTypes.object.isRequired,
    submitSucceeded: PropTypes.bool.isRequired,
    submitting: PropTypes.bool.isRequired,
  };

  constructor() {
    super();
    this.handleRecoverPassword = this.handleRecoverPassword.bind(this);
  }

  handleRecoverPassword(values) {
    const { dispatch, params, router } = this.props;
    const token = params.code;
    const password = values.get('password');

    return dispatch(recoverPasswordFetch(token, password, () =>
      setTimeout(() => router.push('/login'), REDIRECTION)
    ));
  }

  render() {
    const { error, handleSubmit, submitSucceeded, submitting } = this.props;

    return (
      <div>
        <div className="login_wrapper">
          <div className="animate form login_form">
            <section className="login_content">
              <Form
                onSubmit={handleSubmit(this.handleRecoverPassword)}
                noValidate
              >
                <FormGroup>
                  <Field
                    name="password"
                    component={Input}
                    label="Nova zaporka"
                    type="password"
                    placeholder="Nova zaporka"
                  />
                  <Field
                    name="confirmation"
                    component={Input}
                    label="Potvrdi novu zaporku"
                    type="password"
                    placeholder="Potvrdi novu zaporku"
                  />
                  {submitSucceeded &&
                    <SuccessMsg>
                      Zaporka promjenjena. Preusmjeravanje...
                    </SuccessMsg>}
                  {error && <ErrorMsg>{error}</ErrorMsg>}
                  <Button type="submit" disabled={submitting}>Promjeni</Button>
                </FormGroup>
              </Form>
            </section>
          </div>
        </div>
      </div>
    );
  }
}

export default connect()(withRouter(
  reduxForm({
    form: 'ChangePassword',
    validate,
  })(RecoverPasswordComponent)
));
