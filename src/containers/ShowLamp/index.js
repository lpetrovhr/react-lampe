import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { Grid, Row, Col, Table } from 'react-bootstrap';
import { fetchSingleLamp } from '../../actions/lamps';
import { STATUS_ONLINE } from '../../constants/application';

export class ShowLampComponent extends Component {
  static propTypes = {
    dispatch: PropTypes.func.isRequired,
    params: PropTypes.object.isRequired,
    zones: PropTypes.object.isRequired,
    lamps: PropTypes.object.isRequired,
  };

  componentWillMount() {
    const { zones } = this.props;

    const zoneId = zones.getIn(['currentZone', 'id']);

    if (zoneId) {
      this.getLamp(zoneId);
    }
  }

  componentWillReceiveProps(nextProps) {
    const nextZoneId = nextProps.zones.getIn(['currentZone', 'id']);
    const currentZoneId = this.props.zones.getIn(['currentZone', 'id']);
    if (currentZoneId !== nextZoneId) {
      this.getLamp(nextZoneId);
    }
  }

  getLamp = (zoneId) => {
    const { dispatch, params: { lampId } } = this.props;

    dispatch(fetchSingleLamp(zoneId, lampId));
  }

  renderTableData = () => {
    const { lamps } = this.props;
    const { lampsList } = lamps.toJS();

    const working = {
      color: '#005900',
    };

    const notWorking = {
      color: '#B20000',
    };

    if (lampsList.length <= 0) { return null; }

    return (
      <tr className="tableDataAlign" key={lampsList.id}>
        <td>{lampsList.id}</td>
        <td>{lampsList.name}</td>
        <td>
          <span
            style={lampsList.status !== STATUS_ONLINE ? notWorking : working}
            className="fa fa-circle"
          />
        </td>
        <td>
          {lampsList.lampCoord && lampsList.lampCoord.lat}
          - {lampsList.lampCoord && lampsList.lampCoord.lng}
        </td>
        <td>prazno za sad</td>
      </tr>
    );
  };

  render() {
    return (
      <Grid>
        <div className="title_left"><h3>Svjetiljke</h3></div>
        <div className="x_panel">
          <Row className="row">
            <Col md={12} sm={12} xs={12}>
              <Table bordered striped condensed hover responsive>
                <thead>
                  <tr>
                    <th>ID</th>
                    <th>Ime</th>
                    <th>Status</th>
                    <th>Geografska širina - dužina</th>
                    <th>Grupa</th>
                  </tr>
                </thead>
                <tbody>
                  {this.renderTableData()}
                </tbody>
              </Table>
            </Col>
          </Row>
        </div>
      </Grid>
    );
  }
}

export default connect(state => ({
  user: state.get('user'),
  zones: state.get('zones'),
  lamps: state.get('lamps'),
}))(ShowLampComponent);
