import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import store from 'store';
import { logoutAction, getUserData } from '../../actions/auth';
import Header from '../../components/Header';
import Sidebar from '../../components/Sidebar';
import '../../styles/custom.css';

export class AppComponent extends Component {
  static propTypes = {
    children: PropTypes.element.isRequired,
    dispatch: PropTypes.func.isRequired,
    router: PropTypes.object.isRequired,
    user: PropTypes.object.isRequired,
    zones: PropTypes.object.isRequired,
  };

  constructor() {
    super();
    this.handleLogout = this.handleLogout.bind(this);
  }

  componentWillMount() {
    const { dispatch } = this.props;

    if (store.get('token')) {
      dispatch(getUserData());
    }
  }

  handleLogout(e) {
    e.preventDefault();
    const { dispatch, router } = this.props;
    dispatch(logoutAction(router));
  }

  render() {
    const { children, user, zones } = this.props;
    const currentUser = user.toJS();
    const { currentZone } = zones.toJS();

    return (
      <div className="container body">
        <div className="main_container">
          <Header />
          <Sidebar user={currentUser} zone={currentZone} />
          <div
            className="right_col"
            role="main"
            style={{ minHeight: '100vh', paddingTop: '3%' }}
          >
            {children}
          </div>
        </div>
      </div>
    );
  }
}

export default connect(state => ({
  user: state.get('user'),
  zones: state.get('zones'),
}))(withRouter(AppComponent));
