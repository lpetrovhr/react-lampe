import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { Field, reduxForm } from 'redux-form/immutable';
import { Link, withRouter } from 'react-router';
import { Form, FormGroup, Button } from 'react-bootstrap';
import store from 'store';
import { loginFetch } from '../../actions/auth';
import { isEmail, isPassword, isRequired } from '../../utils/validator';
import Input from '../../components/Input';
import ErrorMsg from '../../components/ErrorMsg';

export const validate = (values) => {
  const errors = {};
  const { email, password } = values.toJS();

  errors.email = isRequired(email, 'hr') || isEmail(email, 'hr');
  errors.password = isRequired(password, 'hr') || isPassword(password, 'hr');
  return errors;
};

export class LoginComponent extends Component {
  static propTypes = {
    dispatch: PropTypes.func.isRequired,
    error: PropTypes.string,
    handleSubmit: PropTypes.func.isRequired,
    router: PropTypes.object.isRequired,
    submitting: PropTypes.bool.isRequired,
  };

  constructor() {
    super();
    this.handleLogin = this.handleLogin.bind(this);
  }

  handleLogin(values) {
    const { dispatch, router } = this.props;
    const nextRoute = store.get('nextRoute') || '/';
    if (nextRoute !== '/') store.remove('nextRoute');
    return dispatch(loginFetch(values)).then(() => router.push(nextRoute));
  }

  render() {
    const { error, handleSubmit, submitting } = this.props;
    let newErrorMsg = error;
    if (error === 'Wrong password') {
      newErrorMsg = 'Kriva zaporka';
    } else if (error === 'User not found') {
      newErrorMsg = 'Korisnik nije pronađen';
    }

    return (
      <div>
        <div className="login_wrapper">
          <div className="animate form login_form">
            <section className="login_content">
              <Form onSubmit={handleSubmit(this.handleLogin)} noValidate>
                <h1>Prijava</h1>
                <FormGroup>
                  <Field
                    name="email"
                    component={Input}
                    label="Email"
                    type="email"
                    placeholder="Email"
                  />
                </FormGroup>
                <FormGroup>
                  <Field
                    name="password"
                    component={Input}
                    label="Lozinka"
                    type="password"
                    placeholder="Lozinka"
                  />
                </FormGroup>
                <FormGroup>
                  {error && <ErrorMsg>{newErrorMsg}</ErrorMsg>}
                  <Button type="submit" disabled={submitting}>Prijava</Button>
                </FormGroup>
              </Form>
              <div>
                <Link style={{ color: '#E7E7E7' }} to="/forgotPassword">
                  Zaboravljena lozinka?
                </Link>
              </div>
            </section>
          </div>
        </div>
      </div>
    );
  }
}

export default connect()(withRouter(
  reduxForm({
    form: 'Login',
    validate,
  })(LoginComponent)
));
