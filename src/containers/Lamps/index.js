import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import { Grid, Row, Col, Table } from 'react-bootstrap';
import { fetchLamps, paginationPageChanged } from '../../actions/lamps';
import LampStatus from '../../components/LampStatus';
import Pagination from '../../components/Pagination';
import { ITEMS_PER_PAGE, STATUS_ONLINE } from '../../constants/application';

export class LampsComponent extends Component {
  static propTypes = {
    dispatch: PropTypes.func.isRequired,
    zones: PropTypes.object.isRequired,
    lamps: PropTypes.object.isRequired,
    location: PropTypes.object.isRequired,
    router: PropTypes.object.isRequired,
  };

  constructor() {
    super();
    this.getLamps = this.getLamps.bind(this);
    this.statusChange = this.statusChange.bind(this);
    this.paginationPageChanged = this.paginationPageChanged.bind(this);
    this.getPaginatedList = this.getPaginatedList.bind(this);
    this.onPaginationPageChanged = this.onPaginationPageChanged.bind(this);
  }

  componentWillMount() {
    const { location: { query }, zones } = this.props;
    let { status } = query;
    if (status === undefined) {
      status = 'all';
    }

    const zoneId = zones.getIn(['currentZone', 'id']);

    if (zoneId) {
      this.paginationPageChanged(status, 1, zoneId);
    }
  }

  componentWillReceiveProps(nextProps) {
    const { location: { query } } = this.props;
    let status = nextProps.location.query.status;
    const nextZoneId = nextProps.zones.getIn(['currentZone', 'id']);
    const currentZoneId = this.props.zones.getIn(['currentZone', 'id']);

    if (query.status !== status || currentZoneId !== nextZoneId) {
      if (status === undefined) {
        status = 'all';
      }
      this.paginationPageChanged(status, 1, nextZoneId);
    }
  }

  onPaginationPageChanged = (page, zoneId) => {
    const { dispatch } = this.props;

    dispatch(paginationPageChanged(page, zoneId));
  };

  getLamps = (status, page, zoneId, cb) => {
    const { dispatch, router, zones } = this.props;
    const { currentZone } = zones.toJS();
    dispatch(fetchLamps(status, page, zoneId || currentZone.id, cb))
        .then(() => router.push(`/lamps?status=${status}`));
  };

  getPaginatedList = (status, page, zoneId) => {
    this.getLamps(status, page, zoneId, () => {
      this.onPaginationPageChanged(page, zoneId);
    });
  };

  paginationPageChanged = (status, page, zoneId) => {
    this.getPaginatedList(status, page, zoneId);
  };

  statusChange = (event) => {
    this.getLamps(event.target.value, 1);
  };

  renderTableData = () => {
    const { lamps } = this.props;
    const { lampsList } = lamps.toJS();
    const lampsListRow = lampsList.rows;

    const working = {
      color: '#005900',
    };

    const notWorking = {
      color: '#B20000',
    };

    return lampsListRow && lampsListRow.map(marker =>
      <tr className="tableDataAlign" key={marker.id}>
        <td>{marker.id}</td>
        <td>{marker.name}</td>
        <td>
          <span
            style={marker.status !== STATUS_ONLINE ? notWorking : working}
            className="fa fa-circle"
          />
        </td>
        <td>
          {marker.lampCoord.lat} - {marker.lampCoord.lng}
        </td>
        <td>{marker.group && marker.group.name}</td>
      </tr>
    );
  };

  render() {
    const { lamps, location } = this.props;
    const { currentPage, lampsTotal } = lamps.toJS();
    const status = location.query.status;

    return (
      <Grid>
        <div className="title_left"><h3>Svjetiljke</h3></div>
        <div className="x_panel">
          <Row className="row">
            <div className="x_title">
              <LampStatus
                lampStatus={status}
                statusChange={this.statusChange}
              />
            </div>
            <Col md={12} sm={12} xs={12}>
              {lampsTotal > 0 ?
                <Table bordered striped condensed hover responsive>
                  <thead>
                    <tr>
                      <th>ID</th>
                      <th>Ime</th>
                      <th>Status</th>
                      <th>Geografska širina - dužina</th>
                      <th>Grupa</th>
                    </tr>
                  </thead>
                  <tbody>
                    {this.renderTableData()}
                  </tbody>
                </Table> : <p>Trenutno ne postoje svjetiljke za ovu zonu.</p>}
            </Col>
          </Row>
          {lampsTotal > 0 ?
            <Pagination
              currentPage={currentPage}
              itemsPerPage={ITEMS_PER_PAGE}
              total={lampsTotal}
              onPaginationChange={this.paginationPageChanged}
            /> : null}
        </div>
      </Grid>
    );
  }
}

export default connect(state => ({
  user: state.get('user'),
  zones: state.get('zones'),
  lamps: state.get('lamps'),
}))(withRouter(LampsComponent));
