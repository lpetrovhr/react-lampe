import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { Field, reduxForm } from 'redux-form/immutable';
import { Link } from 'react-router';
import { Form, FormGroup, Button } from 'react-bootstrap';
import { forgotPasswordFetch } from '../../actions/auth';
import { isEmail, isRequired } from '../../utils/validator';
import Input from '../../components/Input';
import ErrorMsg from '../../components/ErrorMsg';
import SuccessMsg from '../../components/SuccessMsg';

export const validate = (values) => {
  const errors = {};
  const { email } = values.toJS();

  errors.email = isRequired(email, 'hr') || isEmail(email, 'hr');
  return errors;
};

export class ForgotPasswordComponent extends Component {
  static propTypes = {
    dispatch: PropTypes.func.isRequired,
    error: PropTypes.string,
    handleSubmit: PropTypes.func.isRequired,
    submitSucceeded: PropTypes.bool.isRequired,
    submitting: PropTypes.bool.isRequired,
  };

  constructor() {
    super();
    this.handleSend = this.handleSend.bind(this);
  }

  handleSend(values) {
    const { dispatch } = this.props;
    return dispatch(forgotPasswordFetch(values));
  }

  render() {
    const { error, handleSubmit, submitSucceeded, submitting } = this.props;

    return (
      <div>
        <div className="login_wrapper">
          <div className="animate form login_form">
            <section className="login_content">
              <Form onSubmit={handleSubmit(this.handleSend)} noValidate>
                <FormGroup>
                  <Field
                    name="email"
                    component={Input}
                    label="E-mail"
                    type="email"
                    placeholder="E-mail"
                  />
                  {submitSucceeded && !submitting &&
                    <SuccessMsg>E-mail je poslan.</SuccessMsg>}
                  {error && <ErrorMsg>{error}</ErrorMsg>}
                  <Button type="submit" disabled={submitting}>Pošalji</Button>
                </FormGroup>
              </Form>
              <Link style={{ color: '#E7E7E7' }} to="/login">Prijavi se</Link>
            </section>
          </div>
        </div>
      </div>
    );
  }
}

export default connect()(reduxForm({
  form: 'ForgotPassword',
  validate,
})(ForgotPasswordComponent));
