import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import GoogleMap from 'google-map-react';
import { Grid, Row, Col } from 'react-bootstrap';
import { STATUS_ALL } from '../../constants/application';
import LampPin from '../../components/LampPin';
import LampStatus from '../../components/LampStatus';
import fetchMarkers from '../../actions/maps';

export class MapsComponent extends Component {
  static propTypes = {
    dispatch: PropTypes.func.isRequired,
    zones: PropTypes.object.isRequired,
    maps: PropTypes.object,
  };

  constructor(props) {
    super(props);
    this.updateMarkers = this.updateMarkers.bind(this);
    this.statusChange = this.statusChange.bind(this);
  }

  state = {
    lampStatus: STATUS_ALL,
  }

  componentWillUnmount() {
    clearTimeout(this.state.timer);
  }

  onChange = ({ bounds }) => {
    this.setState({ bounds });
    this.updateMarkers(bounds, this.state.lampStatus);
  }

  statusChange = (event) => {
    this.setState({ lampStatus: event.target.value });

    this.updateMarkers(this.state.bounds, event.target.value);
  }

  updateMarkers = (bounds, status) => {
    if (this.timer) { clearTimeout(this.timer); }
    const { dispatch, zones } = this.props;
    const { currentZone } = zones.toJS();
    dispatch(fetchMarkers(bounds, status, currentZone.id));
    this.timer = setTimeout(
      () => this.updateMarkers(bounds, status), 300000);
    this.setState({ timer: this.timer });
  }

  renderMarkerData = () => {
    const { maps } = this.props;
    const { data } = maps.toJS();
    const markersData = data.rows;

    if (typeof markersData === 'undefined') { return null; }

    return markersData && markersData.map((marker, key) =>
      <LampPin
        lat={marker.lampCoord.lat}
        lng={marker.lampCoord.lng}
        id={marker.id}
        status={marker.status}
        name={marker.name}
        key={key}
      />
    );
  }

  render() {
    const { maps, zones } = this.props;
    const { mapInfo } = maps.toJS();
    const { currentZone } = zones.toJS();
    const lat = currentZone.zoneCoord
      ? currentZone.zoneCoord.coordinates[1] : null;
    const lng = currentZone.zoneCoord
      ? currentZone.zoneCoord.coordinates[0] : null;
    const mapCenter = [lat, lng];
    const isMap = true;

    const mapStyle = {
      width: '100%',
      height: '50%',
    };

    // TODO fetch center and zoom from user state when db is ready

    return (
      <Grid>
        <LampStatus
          statusChange={this.statusChange}
          isMap={isMap}
        />
        <Row className="row">
          <Col md={12} sm={12} xs={12}>
            <div className="mapWrapper">
              <GoogleMap
                onChange={this.onChange}
                onClick={this.onClick}
                style={mapStyle}
                center={mapCenter}
                zoom={mapInfo.zoom}
              >
                {this.renderMarkerData()}
              </GoogleMap>
            </div>
          </Col>
        </Row>
      </Grid>
    );
  }
}

export default connect(state => ({
  user: state.get('user'),
  zones: state.get('zones'),
  maps: state.get('maps'),
}))(MapsComponent);
