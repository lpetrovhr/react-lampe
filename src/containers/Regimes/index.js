import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router';
import { ResponsiveContainer, BarChart, Bar, XAxis, YAxis,
  CartesianGrid, Tooltip, Legend } from 'recharts';
import { Grid, Row, Col, Clearfix } from 'react-bootstrap';
import { fetchRegimes } from '../../actions/regimes';

export class RegimesComponent extends Component {
  static propTypes = {
    dispatch: PropTypes.func.isRequired,
    regimes: PropTypes.object,
    zones: PropTypes.object.isRequired,
  };

  constructor() {
    super();
    this.renderGraph = this.renderGraph.bind(this);
    this.fetchRegimes = this.fetchRegimes.bind(this);

    this.state = {
      chosenId: null,
    };
  }

  componentWillMount() {
    const { zones } = this.props;

    const zoneId = zones.getIn(['currentZone', 'id']);

    if (zoneId) {
      this.fetchRegimes(zoneId);
    }
  }

  componentWillReceiveProps(nextProps) {
    const nextZoneId = nextProps.zones.getIn(['currentZone', 'id']);
    const currentZoneId = this.props.zones.getIn(['currentZone', 'id']);
    if (currentZoneId !== nextZoneId) {
      this.fetchRegimes(nextZoneId);
    }
  }

  fetchRegimes(id) {
    const { dispatch } = this.props;

    dispatch(fetchRegimes(id));
  }

  renderGraph = (event) => {
    event.preventDefault();
    const { chosenId } = this.state;

    const id = parseInt(event.target.id, 10);
    const nextValue = chosenId === id ? null : id;

    this.setState({
      chosenId: nextValue,
    });
  };

  renderRegimesList = () => {
    const { regimes } = this.props;
    const { regimesList } = regimes.toJS();

    const toPercent = (number, fixed = 0) => `${number.toFixed(fixed)}%`;

    const hiddenGraph = {
      display: 'none',
    };

    const { chosenId } = this.state;

    return regimesList && regimesList.map(re =>
      <div key={re.id}>
        <h2>
          <Link id={re.id} onClick={this.renderGraph}>
            <i className="fa fa-bar-chart" /> {re.name}
          </Link>
        </h2>
        {chosenId === re.id && <div
          style={re.id === chosenId ? null : hiddenGraph}
          id={`graph_${re.id}`}
          className="regimes-graph--container"
        >
          <ResponsiveContainer>
            <BarChart
              data={re.schedule}
              stackOffset="expand"
              margin={{ top: 10, right: 30, left: 20, bottom: 10 }}
            >
              <XAxis dataKey="name" />
              <YAxis
                domain={[0, 100]}
                ticks={[0, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100]}
                tickFormatter={toPercent}
              />
              <CartesianGrid />
              <Tooltip />
              <Legend />
              <Bar dataKey="value" name="Jačina" fill="#1ABB9C" />
            </BarChart>
          </ResponsiveContainer>
        </div>}
      </div>
    );
  };

  render() {
    const { regimes } = this.props;
    const { regimesCount } = regimes.toJS();

    return (
      <Grid>
        <div className="title_left"><h3>Režimi</h3></div>
        <div className="x_panel">
          <Row className="row">
            <Col md={12} sm={12} xs={12}>
              <div className="x_title">
                <h2>Popis režima</h2>
                <Clearfix />
              </div>
              <div className="x_content">
                {regimesCount > 0 ? this.renderRegimesList()
                  : <p>Trenutno nema režima za ovu zonu.</p>}
              </div>
            </Col>
          </Row>
        </div>
      </Grid>
    );
  }
}

export default connect(state => ({
  user: state.get('user'),
  regimes: state.get('regimes'),
  zones: state.get('zones'),
}))(RegimesComponent);
