import { NOTIFICATIONS_FETCH_SUCCESS,
  NOTIFICATIONS_LAMPS_FETCH_SUCCESS } from '../constants/actions';
import { API_URL } from '../constants/application';
import fetch from '../utils/fetch';
import parseErrors from '../utils/parseErrors';


export const updateNotifications = notificationCount => ({
  type: NOTIFICATIONS_FETCH_SUCCESS,
  notificationCount,
});

export const notificationsLampsFetchSuccess = lampsData => ({
  type: NOTIFICATIONS_LAMPS_FETCH_SUCCESS,
  lampsData,
});

export const getNotifications = (status, zoneId) =>
  dispatch =>
    fetch(`${API_URL}/zones/${zoneId}/lampCount?status=${status}`)
    .then((resp) => {
      dispatch(updateNotifications(resp.count));
    }).catch(err =>
      Promise.reject(parseErrors(err)),
    );

export const getOfflineLamps = (status, zoneId) =>
  dispatch =>
    fetch(`${API_URL}/zones/${zoneId}/lamps?status=${status}&page=1`)
      .then((resp) => {
        dispatch(notificationsLampsFetchSuccess(resp.rows));
      }).catch(err =>
        Promise.reject(parseErrors(err)),
      );
