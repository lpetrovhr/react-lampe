import { USER_ZONES_FETCH_SUCCESS, USER_CURRENT_ZONE_CHANGE_SUCCESS,
  ZONE_USERS_FETCH_SUCCESS } from '../constants/actions';
import { API_URL, STATUS_ALL, STATUS_OFFLINE } from '../constants/application';
import { fetchLamps } from './lamps';
import { groupsGetFetch } from './groups';
import { fetchRegimes } from './regimes';
import { getNotifications } from './notifications';
import fetch from '../utils/fetch';
import parseErrors from '../utils/parseErrors';

export const userZonesFetchSuccess = zones => ({
  type: USER_ZONES_FETCH_SUCCESS,
  zonesData: zones,
  currentZoneData: zones[0],
});

export const userCurrentZoneChange = zone => ({
  type: USER_CURRENT_ZONE_CHANGE_SUCCESS,
  currentZoneData: zone,
});

export const zoneUsersFetchSuccess = users => ({
  type: ZONE_USERS_FETCH_SUCCESS,
  usersData: users,
});

export const fetchUserZones = userId =>
  dispatch =>
    fetch(`${API_URL}/users/${userId}/zones`)
      .then(resp =>
        dispatch(userZonesFetchSuccess(resp.rows)),
      ).catch(err =>
        Promise.reject(parseErrors(err)),
      );

export const fetchZoneUsers = zoneId =>
  dispatch =>
    fetch(`${API_URL}/zones/${zoneId}/users`)
      .then(resp =>
        dispatch(zoneUsersFetchSuccess(resp.rows)),
      ).catch(err =>
        Promise.reject(parseErrors(err)),
      );

export const changeCurrentZone = zone =>
  (dispatch) => {
    dispatch(fetchLamps(STATUS_ALL, 1, zone.id));
    dispatch(groupsGetFetch(zone.id));
    dispatch(fetchRegimes(zone.id));
    dispatch(getNotifications(STATUS_OFFLINE, zone.id));
    if (zone.userZone.role === 'admin') {
      dispatch(fetchZoneUsers(zone.id));
    }
    return dispatch(userCurrentZoneChange(zone));
  };
