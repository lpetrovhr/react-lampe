import { GROUPS_FETCH_SUCCESS, GROUP_REGIME_CHANGE_SUCESS,
  GROUP_REGIME_CHANGE_FAIL } from '../constants/actions';
import { API_URL } from '../constants/application';
import fetch from '../utils/fetch';
import parseErrors from '../utils/parseErrors';

export const groups = groupsData => ({
  type: GROUPS_FETCH_SUCCESS,
  groupsData: groupsData.rows,
  count: groupsData.count,
});

export const setRegimeSuccess = () => ({
  type: GROUP_REGIME_CHANGE_SUCESS,
});

export const setRegimeFail = err => ({
  type: GROUP_REGIME_CHANGE_FAIL,
  error: err,
});

export const groupsGetFetch = zoneId =>
  dispatch =>
    fetch(`${API_URL}/zones/${zoneId}/groups`).then((resp) => {
      dispatch(groups(resp));
    }).catch(err =>
      Promise.reject(parseErrors(err)),
    );

export const groupsSetRegime = (regimeId, groupId, zoneId) =>
  dispatch =>
    fetch(`${API_URL}/zones/${zoneId}/groups/${groupId}/setRegime`, {
      method: 'POST',
      body: JSON.stringify({ regimeId }),
    }).then(resp =>
        dispatch(setRegimeSuccess(resp)),
      ).catch(err =>
        dispatch(setRegimeFail(err))
      );
