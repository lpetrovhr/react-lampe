import { GET_REGIMES } from '../constants/actions';
import { API_URL, INTERVALS } from '../constants/application';
import fetch from '../utils/fetch';
import parseErrors from '../utils/parseErrors';

export const regimes = (regimesData, count) => ({
  type: GET_REGIMES,
  regimesData,
  count,
});

const percent = (value) => {
  const maxValue = 255;
  const calculation = (value * 100) / maxValue;
  const result = Math.round(calculation);

  return result;
};

const convert = (responseData) => {
  const newResponse = [];

  responseData.forEach((element) => {
    const newData = {};
    const newSchedule = [];
    newData.id = element.id;
    newData.name = element.name;

    for (let i = 0; i < 48; i += 1) {
      newSchedule[i] = { name: INTERVALS[i],
        value: percent(element.schedule[i]) };
    }

    newData.schedule = newSchedule;
    newResponse.push(newData);
  });

  return newResponse;
};

export const fetchRegimes = zoneId =>
   dispatch =>
    fetch(`${API_URL}/zones/${zoneId}/regimes`)
    .then((resp) => {
      dispatch(regimes(convert(resp.rows), resp.count));
    }).catch(err =>
      Promise.reject(parseErrors(err)),
    );
