import store from 'store';
import { SIGNUP_LOGIN_SUCCESS, LOGOUT_SUCCESS, NEW_EMAIL_CONFIRM_SUCCESS,
  EMAIL_CONFIRM_SUCCESS, EMAIL_CONFIRM_FAILED } from '../constants/actions';
import { API_URL } from '../constants/application';
import parseErrors from '../utils/parseErrors';
import fetch from '../utils/fetch';
import { userZonesFetchSuccess } from './zones';

export const signupLoginSuccess = user => ({
  type: SIGNUP_LOGIN_SUCCESS,
  user,
});

export const logoutSuccess = () => ({
  type: LOGOUT_SUCCESS,
});

export const newEmailConfirmSuccess = (user, deleteKey) => ({
  type: NEW_EMAIL_CONFIRM_SUCCESS,
  user,
  deleteKey,
});

export const emailConfirmSuccess = user => ({
  type: EMAIL_CONFIRM_SUCCESS,
  user,
});

export const emailConfirmFailed = error => ({
  type: EMAIL_CONFIRM_FAILED,
  error,
});

const getUserZones = userId =>
  dispatch =>
    fetch(`${API_URL}/users/${userId}/zones`)
      .then(resp =>
        dispatch(userZonesFetchSuccess(resp.rows))
      ).catch(err =>
        Promise.reject(parseErrors(err)),
      );

export const authenticate = values =>
  dispatch =>
    fetch(`${API_URL}/authenticate`, {
      method: 'POST',
      body: JSON.stringify(values),
      noAuth: true,
    }).then((response) => {
      store.set('token', `Bearer ${response.token}`);
      store.set('user', response.user);
      dispatch(signupLoginSuccess(response.user));
      return dispatch(getUserZones(response.user.id));
    });

export const getUserData = () =>
  dispatch =>
    fetch(`${API_URL}/user`, {
      method: 'GET',
    }).then((response) => {
      store.set('user', response);
      dispatch(signupLoginSuccess(response));
      return dispatch(getUserZones(response.id));
    });

export const loginFetch = values =>
  dispatch =>
    dispatch(authenticate(values)).catch(err =>
        Promise.reject(parseErrors(err))
      );

export const logoutAction = cb =>
  (dispatch) => {
    store.remove('token');
    dispatch(logoutSuccess());
    return typeof cb === 'function' && cb();
  };

export function forgotPasswordFetch(values) {
  return () =>
    fetch(`${API_URL}/resetPassword`, {
      method: 'POST',
      body: JSON.stringify(values),
    }).catch(err =>
      Promise.reject(parseErrors(err))
    );
}

export function recoverPasswordFetch(token, password, callback) {
  return () =>
    fetch(`${API_URL}/recoverPassword/${token}`, {
      method: 'POST',
      body: JSON.stringify({ password }),
    }).then(() => {
      if (typeof callback === 'function') callback();
    }).catch(err =>
      Promise.reject(parseErrors(err))
    );
}

export function emailConfirmFetch(values, callback) {
  return dispatch =>
    fetch(`${API_URL}/emailConfirm`, {
      method: 'POST',
      body: JSON.stringify(values),
    }).then(() => {
      const user = store.get('user');
      if (user.newEmail) {
        user.email = user.newEmail;
        delete user.newEmail;
        dispatch(newEmailConfirmSuccess(user, 'newEmail'));
      } else {
        user.confirmed = true;
        dispatch(emailConfirmSuccess(user));
      }
      store.set('user', user);
      if (typeof callback === 'function') callback();
    }).catch(err =>
      dispatch(emailConfirmFailed(err.message))
    );
}

export function emailResendFetch(values) {
  return () =>
    fetch(`${API_URL}/resendConfirmation`, {
      method: 'POST',
      body: JSON.stringify(values),
    });
}
