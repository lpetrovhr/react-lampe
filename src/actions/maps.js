import { QUERY_MAP } from '../constants/actions';
import { API_URL } from '../constants/application';
import fetch from '../utils/fetch';
import parseErrors from '../utils/parseErrors';


const query = markersData => ({
  type: QUERY_MAP,
  markersData,
});

export default function fetchMarkers(bounds, status, zoneId) {
  const nwLng = bounds.nw.lng;
  const nwLat = bounds.nw.lat;
  const seLng = bounds.se.lng;
  const seLat = bounds.se.lat;
  const geo = `nwLng=${nwLng}&nwLat=${nwLat}&seLng=${seLng}&seLat=${seLat}`;
  const lampStatus = status === 'all' ? '' : `&status=${status}`;

  return dispatch =>
    fetch(`${API_URL}/zones/${zoneId}/lamps?${geo}${lampStatus}`, {
      method: 'GET',
    }).then((resp) => {
      dispatch(query(resp));
    }).catch(err =>
      Promise.reject(parseErrors(err)),
    );
}
