import { GET_LAMPS, LAMPS_PAGINATION_CHANGED,
  GROUP_LAMPS_FETCH_SUCCESS, GROUP_LAMPS_FETCH_FAIL,
  SINGLE_LAMP_FETCH_SUCCESS } from '../constants/actions';
import { API_URL } from '../constants/application';
import fetch from '../utils/fetch';
import parseErrors from '../utils/parseErrors';

const lamps = lampsData => ({
  type: GET_LAMPS,
  lampsData,
  lampsTotal: lampsData.count,
});

const lampFetchSuccess = lampData => ({
  type: SINGLE_LAMP_FETCH_SUCCESS,
  lampData,
});

export const groupLampsFetchSuccess = lampsData => ({
  type: GROUP_LAMPS_FETCH_SUCCESS,
  lampsData,
});

export const groupLampsFetchFail = error => ({
  type: GROUP_LAMPS_FETCH_FAIL,
  error,
});

export const paginationPageChanged = page => ({
  type: LAMPS_PAGINATION_CHANGED,
  currentPage: page,
});

export const fetchLamps = (status, currentPage, zoneId, callback = null) => {
  let search = '';
  if (status !== 'all') search = `status=${status}&`;
  return dispatch =>
    fetch(`${API_URL}/zones/${zoneId}/lamps?${search}page=${currentPage}`, {
      method: 'GET',
    }).then((resp) => {
      dispatch(lamps(resp));
      if (typeof callback === 'function') callback();
    }).catch(err =>
      Promise.reject(parseErrors(err)),
    );
};

export const groupLampsFetch =
  (zoneId, groupId, currentPage, status, callback = null) => {
    let search = `&page=${currentPage}`;
    if (status !== 'all') search = `&status=${status}&page=${currentPage}`;

    return dispatch =>
      fetch(`${API_URL}/zones/${zoneId}/groups/${groupId}/lamps?${search}`)
      .then((resp) => {
        dispatch(groupLampsFetchSuccess(resp));
        if (typeof callback === 'function') callback();
      }).catch(err =>
        dispatch(groupLampsFetchFail(err)),
      );
  };

export const fetchSingleLamp = (zoneId, lampId) =>
  dispatch =>
    fetch(`${API_URL}/zones/${zoneId}/lamps/${lampId}`)
      .then((resp) => {
        dispatch(lampFetchSuccess(resp));
      }).catch((error) => {
        Promise.reject(parseErrors(error));
      });
