import React, { PropTypes } from 'react';
import { FormControl } from 'react-bootstrap';
import ErrorMsg from '../ErrorMsg';

const Input = (props) => {
  const { label, input, meta: { active, touched, error }, ...other } = props;

  return (
    <div>
      <label htmlFor={input.name}>
        {label}
        <FormControl id={input.name} {...input} {...other} />
      </label>
      {!active && touched && error && <ErrorMsg>{error}</ErrorMsg>}
    </div>
  );
};

Input.propTypes = {
  label: PropTypes.string,
  input: PropTypes.object.isRequired,
  meta: PropTypes.object.isRequired,
};

export default Input;
