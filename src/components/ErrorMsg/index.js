import React, { PropTypes } from 'react';

const error = {
  backgroundColor: '#ffbbbb',
  color: '#791b23',
  margin: '5px 0 15px',
  padding: '6.5px 10px',
  textAlign: 'center',
};

const icon = {
  marginRight: 5,
};

const ErrorMsg = ({ children }) =>
  <p style={error}>
    <i style={icon} className="fa fa-exclamation-triangle" />
    {children}
  </p>;

ErrorMsg.propTypes = {
  children: PropTypes.string.isRequired,
};

export default ErrorMsg;
