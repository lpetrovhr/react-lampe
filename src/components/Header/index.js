import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { withRouter, Link } from 'react-router';
import { ButtonGroup, Button, Dropdown, MenuItem } from 'react-bootstrap';
import { logoutAction } from '../../actions/auth';
import { getNotifications } from '../../actions/notifications';
import { changeCurrentZone } from '../../actions/zones';
import { STATUS_OFFLINE } from '../../constants/application';

function toggleMenu() {
  const body = document.getElementsByTagName('body');
  const navigation = body[0].classList[0];
  const navigationList = body[0].classList;

  if (navigation === 'nav-md') {
    navigationList.remove('nav-md');
    navigationList.add('nav-sm');
  } else {
    navigationList.remove('nav-sm');
    navigationList.add('nav-md');
  }
}

export class HeaderComponent extends Component {
  static propTypes = {
    dispatch: PropTypes.func.isRequired,
    notifications: PropTypes.object,
    zones: PropTypes.object.isRequired,
    router: PropTypes.object.isRequired,
  };

  constructor() {
    super();
    this.fetchNotifications = this.fetchNotifications.bind(this);

    this.state = {};
  }

  componentWillMount() {
    const { zones } = this.props;

    const zoneId = zones.getIn(['currentZone', 'id']);

    if (zoneId) {
      this.fetchNotifications(zoneId);
    }
  }

  componentWillReceiveProps(nextProps) {
    const nextZoneId = nextProps.zones.getIn(['currentZone', 'id']);
    const currentZoneId = this.props.zones.getIn(['currentZone', 'id']);
    if (currentZoneId !== nextZoneId) {
      this.fetchNotifications(nextZoneId);
    }
  }

  componentWillUnmount() {
    clearTimeout(this.state.timer);
  }

  onZoneChange = (event) => {
    const { dispatch, zones } = this.props;
    const { zonesList } = zones.toJS();
    const zoneId = parseInt(event.target.value, 10);
    let newZone = {};

    zonesList.forEach((zone) => {
      if (zone.id === zoneId) {
        newZone = zone;
      }
    });

    dispatch(changeCurrentZone(newZone));
  }

  onNotificationClick = () => {
    const { router } = this.props;
    router.push('/lamps?status=offline');
  }

  handleLogout = (e) => {
    const { dispatch, router } = this.props;

    e.preventDefault();
    dispatch(logoutAction(() => router.push('/login')));
  }

  fetchNotifications = (zoneId) => {
    const { dispatch } = this.props;
    if (this.timer) { clearTimeout(this.timer); }
    dispatch(getNotifications(STATUS_OFFLINE, zoneId));

    this.timer = setTimeout(
      () => this.fetchNotifications(zoneId), 300000);
    this.setState({ timer: this.timer });
  }

  renderZoneSelectOptions = () => {
    const { zones } = this.props;
    const { zonesList } = zones.toJS();

    return zonesList && zonesList.map(zone =>
      <option value={zone.id} key={zone.id}>{zone.name}</option>,
    );
  }

  renderNotificationItems = lampsList =>
    lampsList && lampsList.map(lamp =>
      <MenuItem
        onClick={() => this.onMenuItemClick(lamp.id)}
        key={lamp.id}
      >
        {lamp.name}
      </MenuItem>
    );

  render() {
    const { notifications, zones } = this.props;
    const { notificationCount } = notifications.toJS();
    const { currentZone } = zones.toJS();

    return (
      <div className="top_nav">
        <div className="nav_menu">
          <nav>
            <div className="nav toggle">
              <Link
                id="menu_toggle"
                title="Menu"
                onClick={() => { toggleMenu(); }}
              >
                <i className="fa fa-bars" />
              </Link>
            </div>
            <ul className="nav navbar-nav navbar-right">
              <li>
                <ButtonGroup className="header-button-group">
                  <Dropdown
                    className="header-button-group--button"
                    id="alerts-dropdown"
                    title="Obavijesti"
                  >
                    <Dropdown.Toggle className="header-button-group--button">
                      <i className="fa fa-bell-o fa-lg" />
                      {notificationCount > 0 ?
                        <span className="notification-badge bg-red">
                          {notificationCount}
                        </span> : null
                      }
                    </Dropdown.Toggle>
                    <Dropdown.Menu>
                      {notificationCount > 0 ?
                        <MenuItem
                          eventKey={1}
                          onSelect={this.onNotificationClick}
                        >
                          Prikaži sve lampe koje ne rade
                        </MenuItem>
                        : null}
                    </Dropdown.Menu>
                  </Dropdown>
                  <Button
                    className="header-button-group--button"
                    title="Odjava"
                    onClick={this.handleLogout}
                  >
                    <i className="fa fa-power-off fa-lg" />
                  </Button>
                </ButtonGroup>
              </li>
              <li className="navigation--header-select">
                <select
                  id="zoneSelect"
                  onChange={this.onZoneChange}
                  className="form-control input-sm"
                  defaultValue={currentZone.id}
                >
                  {this.renderZoneSelectOptions()}
                </select>
              </li>
            </ul>
          </nav>
        </div>
      </div>
    );
  }
}

export default connect(state => ({
  user: state.get('user'),
  notifications: state.get('notifications'),
  zones: state.get('zones'),
}))(withRouter(HeaderComponent));
