import React, { PropTypes } from 'react';
import { Col } from 'react-bootstrap';
import { Link } from 'react-router';
import NavLink from '../NavLink';

const Sidebar = (props) => {
  const { user, zone } = props;

  return (
    <Col md={3} className="left_col">
      <div className="left_col scroll-view">
        <div className="navbar nav_title">
          <Link to="/" className="site_title">
            <i className="fa fa-user" />
            <span> {user.firstname} {user.lastname}</span>
          </Link>
        </div>
        <div
          id="sidebar-menu"
          className="main_menu_side hidden-print main_menu"
        >
          <div className="menu_section">
            <ul className="nav side-menu">
              <NavLink to="/maps" activeClassName="active">
                <i className="fa fa-map" /> Karta
              </NavLink>
              <NavLink to="/groups" activeClassName="active">
                <i className="fa fa-bolt" /> Grupe
              </NavLink>
              <NavLink to="/regimes" activeClassName="active">
                <i className="fa fa-battery-full" /> Režimi
              </NavLink>
              <NavLink to="/lamps" activeClassName="active">
                <i className="fa fa-list" /> Svjetiljke
              </NavLink>
              { zone.userZone && zone.userZone.role === 'admin' ?
                <NavLink to="/users" activeClassName="active">
                  <i className="fa fa-group" /> Korisnici
                </NavLink> : null }
            </ul>
          </div>
        </div>
      </div>
    </Col>
  );
};

Sidebar.propTypes = {
  user: PropTypes.object.isRequired,
  zone: PropTypes.object.isRequired,
};

export default Sidebar;
