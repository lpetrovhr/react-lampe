import React, { PropTypes } from 'react';
import { Field, reduxForm } from 'redux-form/immutable';
import { withProps } from 'recompose';
import { Form, FormGroup, Button } from 'react-bootstrap';
import { isEmail, isPassword, isRequired, isUsedEmail }
  from '../../utils/validator';
import Input from '../Input';
import ErrorMsg from '../ErrorMsg';
import SuccessMsg from '../SuccessMsg';

export const validate = (values, { initialValues }) => {
  const { oldEmail, newEmail, password } = values.toJS();
  const currentEmail = initialValues.get('oldEmail');
  const errors = {};

  errors.oldEmail = isRequired(oldEmail, 'hr') || isEmail(oldEmail, 'hr') ||
    isUsedEmail(oldEmail, currentEmail, true, 'hr');
  errors.newEmail = isRequired(newEmail, 'hr') || isEmail(newEmail, 'hr') ||
    isUsedEmail(newEmail, currentEmail, false);
  errors.password = isRequired(password, 'hr') || isPassword(password, 'hr');
  return errors;
};

export const ChangeEmailFormComponent = (props) => {
  const { error, handleChangeEmail, handleSubmit, submitSucceeded,
    submitting } = props;

  return (
    <Form onSubmit={handleSubmit(handleChangeEmail)} noValidate>
      <FormGroup>
        <Field
          name="oldEmail"
          component={Input}
          label="Stari e-mail"
          type="email"
          placeholder="Stari e-mail"
        />
      </FormGroup>
      <FormGroup>
        <Field
          name="newEmail"
          component={Input}
          label="Novi e-mail"
          type="email"
          placeholder="Novi e-mail"
        />
      </FormGroup>
      <FormGroup>
        <Field
          name="password"
          component={Input}
          label="Zaporka"
          type="password"
          placeholder="Zaporka"
        />
      </FormGroup>
      <FormGroup>
        {submitSucceeded && !submitting &&
          <SuccessMsg>Otvorite pristigli e-mail i potvrdite!</SuccessMsg>}
        {error && <ErrorMsg>{error}</ErrorMsg>}
        <Button type="submit" disabled={submitting}>Promjeni</Button>
      </FormGroup>
    </Form>
  );
};

ChangeEmailFormComponent.propTypes = {
  error: PropTypes.string,
  handleChangeEmail: PropTypes.func.isRequired,
  handleSubmit: PropTypes.func.isRequired,
  submitSucceeded: PropTypes.bool.isRequired,
  submitting: PropTypes.bool.isRequired,
};

export default withProps(({ user }) => ({
  initialValues: {
    oldEmail: user.get('email'),
  },
}))(reduxForm({
  form: 'ChangeEmailForm',
  validate,
})(ChangeEmailFormComponent));
