import React, { PropTypes } from 'react';
import { Link } from 'react-router';

const NavLink = ({ to, children, activeClassName }) =>
  <li>
    <Link to={to} activeClassName={activeClassName}>
      {children}
    </Link>
  </li>;

NavLink.propTypes = {
  to: PropTypes.string,
  children: PropTypes.array,
  activeClassName: PropTypes.string,
};

export default NavLink;
