import React, { PropTypes } from 'react';
import { Link } from 'react-router';
import { ButtonToolbar, Button } from 'react-bootstrap';

const MarkerInfo = ({ id, status, name, showInfo, onInfoClick }) =>
  <div className="markerInfo">
    <Link className="markerInfo--close" onClick={showInfo}>
      <i className="fa fa-times" />
    </Link>
    <p className="markerInfo--status">{status}</p>
    <p>ID: {id}</p>
    <p>Naziv: {name}</p>
    <ButtonToolbar>
      <Button onClick={onInfoClick} bsStyle="info" bsSize="xsmall">
        Više informacija
      </Button>
    </ButtonToolbar>
  </div>;

MarkerInfo.propTypes = {
  id: PropTypes.number,
  status: PropTypes.string,
  name: PropTypes.string,
  showInfo: PropTypes.func,
  onInfoClick: PropTypes.func,
};


export default MarkerInfo;
