import React, { PropTypes } from 'react';
import { Field, reduxForm } from 'redux-form/immutable';
import { Form, FormGroup, Button } from 'react-bootstrap';
import { isPassword, isRequired, isSamePassword } from '../../utils/validator';
import Input from '../Input';
import ErrorMsg from '../ErrorMsg';
import SuccessMsg from '../SuccessMsg';

export const validate = (values) => {
  const { oldPassword, newPassword, confirmation } = values.toJS();
  const errors = {};

  errors.oldPassword = isRequired(oldPassword, 'hr') ||
    isPassword(oldPassword, 'hr');
  errors.newPassword = isRequired(newPassword, 'hr') ||
    isPassword(newPassword, 'hr');
  errors.confirmation = isRequired(confirmation, 'hr') ||
    isPassword(confirmation, 'hr') ||
    isSamePassword(confirmation, newPassword, 'hr');
  return errors;
};

export const ChangePasswordFormComponent = (props) => {
  const { error, handleChangePassword, handleSubmit, submitSucceeded,
    submitting } = props;

  return (
    <Form onSubmit={handleSubmit(handleChangePassword)} noValidate>
      <FormGroup>
        <Field
          name="oldPassword"
          component={Input}
          label="Trenutna zaporka"
          type="password"
          placeholder="Trenutna zaporka"
        />
      </FormGroup>
      <FormGroup>
        <Field
          name="newPassword"
          component={Input}
          label="Nova zaporka"
          type="password"
          placeholder="Nova zaporka"
        />
      </FormGroup>
      <FormGroup>
        <Field
          name="confirmation"
          component={Input}
          label="Potvrdi zaporku"
          type="password"
          placeholder="Potvrdi zaporku"
        />
      </FormGroup>
      <FormGroup>
        {submitSucceeded && <SuccessMsg>Zaporka promjenjena.</SuccessMsg>}
        {error && <ErrorMsg>{error}</ErrorMsg>}
        <Button type="submit" disabled={submitting}>Promjeni</Button>
      </FormGroup>
    </Form>
  );
};

ChangePasswordFormComponent.propTypes = {
  error: PropTypes.string,
  handleChangePassword: PropTypes.func.isRequired,
  handleSubmit: PropTypes.func.isRequired,
  submitSucceeded: PropTypes.bool.isRequired,
  submitting: PropTypes.bool.isRequired,
};

export default reduxForm({
  form: 'ChangePasswordForm',
  validate,
})(ChangePasswordFormComponent);
