import React, { PropTypes, Component } from 'react';
import { connect } from 'react-redux';
import { withRouter, Link } from 'react-router';
import MarkerInfo from '../MarkerInfo';

export class LampPinComponent extends Component {
  static propTypes = {
    id: PropTypes.number,
    status: PropTypes.string,
    name: PropTypes.string,
    router: PropTypes.object.isRequired,
  }

  constructor() {
    super();
    this.showInfo = this.showInfo.bind(this);
  }

  state = {
    childVisible: false,
  }

  onInfoClick = (lampId) => {
    const { router } = this.props;
    router.push(`/lamps/${lampId}`);
  }

  showInfo = () => {
    this.setState({ childVisible: !this.state.childVisible });
  }

  render() {
    const { id, status, name } = this.props;

    const styleGreen = {
      position: 'absolute',
      left: -10 / 2,
      top: -10 / 2,
      color: '#005900',
      fontSize: 20,
      cursor: 'pointer',
    };

    const styleRed = {
      position: 'absolute',
      left: -10 / 2,
      top: -10 / 2,
      color: '#B20000',
      fontSize: 20,
      cursor: 'pointer',
    };

    let style = styleGreen;
    if (status === 'offline') style = styleRed;

    return (
      <div>
        <Link onClick={this.showInfo}>
          <span
            style={style}
            className="fa fa-circle"
          />
        </Link>
        { this.state.childVisible ?
          <MarkerInfo
            status={status}
            id={id}
            name={name}
            showInfo={this.showInfo}
            onInfoClick={() => this.onInfoClick(id)}
          /> : null }
      </div>
    );
  }
}

export default connect(state => ({
  user: state.get('users'),
}))(withRouter(LampPinComponent));
