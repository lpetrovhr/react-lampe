import React, { PropTypes } from 'react';
import { Row, Col } from 'react-bootstrap';
import { STATUS_ALL,
  STATUS_ONLINE, STATUS_OFFLINE } from '../../constants/application';

const LampStatus = ({ lampStatus, statusChange, isMap }) =>
  <Row className="row">
    <Col md={3} sm={3} xs={3}>
      <div className={isMap ? 'lampDropdownOnMap' : 'lampDropdown'}>
        <select
          id="lampsSelect"
          onChange={statusChange}
          className="form-control input-sm"
          value={lampStatus}
        >
          <option value={STATUS_ALL}>Sve</option>
          <option value={STATUS_ONLINE}>Ispravne</option>
          <option value={STATUS_OFFLINE}>Neispravne</option>
        </select>
      </div>
    </Col>
  </Row>;

LampStatus.propTypes = {
  lampStatus: PropTypes.string,
  statusChange: PropTypes.func,
  isMap: PropTypes.bool,
};

export default LampStatus;
