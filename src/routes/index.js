import React, { PropTypes } from 'react';
import { Provider } from 'react-redux';
import { Router, Route, IndexRoute, IndexRedirect } from 'react-router';
import store from 'store';
import App from '../containers/App';
import Profile from '../containers/Profile';
import EditProfile from '../containers/EditProfile';
import Login from '../containers/Login';
import ForgotPassword from '../containers/ForgotPassword';
import RecoverPassword from '../containers/RecoverPassword';
import EmailConfirm from '../containers/EmailConfirm';
import Maps from '../containers/Maps';
import Groups from '../containers/Groups';
import GroupLamps from '../containers/GroupLamps';
import Regimes from '../containers/Regimes';
import Statistic from '../containers/Statistic';
import Lamps from '../containers/Lamps';
import ShowLamp from '../containers/ShowLamp';
import Users from '../containers/Users';
import Page404 from '../containers/Page404';

function requireAuth(nextState, replaceState) {
  if (!store.get('token')) {
    store.set('nextRoute', nextState.location.pathname);
    replaceState('/login');
  }
}

function doNotRequireAuth(nextState, replaceState) {
  if (store.get('token')) {
    replaceState('/');
  }
}

const Routes = ({ store: theStore, history }) =>
  <Provider store={theStore}>
    <Router history={history}>
      <Route path="/">
        <Route component={App} onEnter={requireAuth}>
          <IndexRedirect to="/maps" />
          <Route path="/maps" component={Maps} />
          <Route path="groups">
            <IndexRoute component={Groups} />
            <Route path=":groupId/lamps" component={GroupLamps} />
          </Route>
          <Route path="regimes" component={Regimes} />
          <Route path="statistic" component={Statistic} />
          <Route path="lamps">
            <IndexRoute component={Lamps} />
            <Route path=":lampId" component={ShowLamp} />
          </Route>
          <Route path="users" component={Users} />
          <Route path="profile" component={Profile} />
          <Route path="editProfile" component={EditProfile} />
        </Route>
        <Route onEnter={doNotRequireAuth}>
          <Route path="login" component={Login} />
          <Route path="forgotPassword" component={ForgotPassword} />
          <Route path="recoverPassword/:code" component={RecoverPassword} />
          <Route path="emailConfirm/:code" component={EmailConfirm} />
        </Route>
        <Route path="*" component={Page404} />
      </Route>
    </Router>
  </Provider>;

Routes.propTypes = {
  store: PropTypes.object.isRequired,
  history: PropTypes.object.isRequired,
};

export default Routes;
