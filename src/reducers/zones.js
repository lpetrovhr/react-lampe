import { fromJS } from 'immutable';
import { USER_ZONES_FETCH_SUCCESS, USER_CURRENT_ZONE_CHANGE_SUCCESS,
  ZONE_USERS_FETCH_SUCCESS } from '../constants/actions';

const initialState = () =>
  fromJS({
    zonesList: [],
    usersList: [],
    currentZone: {},
  });

export default (state = initialState(), action) => {
  const { zonesData, currentZoneData, usersData } = action;
  switch (action.type) {
    case USER_ZONES_FETCH_SUCCESS: {
      return state
        .set('zonesList', fromJS(zonesData))
        .set('currentZone', fromJS(currentZoneData));
    }
    case USER_CURRENT_ZONE_CHANGE_SUCCESS: {
      return state
        .set('currentZone', fromJS(currentZoneData));
    }
    case ZONE_USERS_FETCH_SUCCESS: {
      return state
        .set('usersList', fromJS(usersData));
    }
    default:
      return state;
  }
};
