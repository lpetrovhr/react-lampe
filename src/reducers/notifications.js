import { fromJS } from 'immutable';
import { NOTIFICATIONS_FETCH_SUCCESS,
  NOTIFICATIONS_LAMPS_FETCH_SUCCESS } from '../constants/actions';

const initialState = fromJS({
  notificationCount: 0,
  lampsList: [],
});

export default (state = initialState, action) => {
  const { notificationCount, lampsData } = action;
  switch (action.type) {
    case NOTIFICATIONS_FETCH_SUCCESS: {
      return state
        .set('notificationCount', fromJS(notificationCount));
    }
    case NOTIFICATIONS_LAMPS_FETCH_SUCCESS: {
      return state
        .set('lampsList', fromJS(lampsData));
    }

    default:
      return state;
  }
};
