import { fromJS } from 'immutable';
import { GET_LAMPS, LAMPS_PAGINATION_CHANGED, GROUP_LAMPS_FETCH_SUCCESS,
  SINGLE_LAMP_FETCH_SUCCESS } from '../constants/actions';

function defaultState() {
  return fromJS({
    error: null,
    lampsList: [],
    currentPage: 1,
  });
}

export default (state = defaultState(), action) => {
  const { lampsData, lampsTotal } = action;
  switch (action.type) {
    case LAMPS_PAGINATION_CHANGED: {
      return state
        .mergeDeep(fromJS({
          currentPage: action.currentPage,
        }));
    }

    case GET_LAMPS: {
      return state
        .set('lampsList', fromJS(lampsData))
        .set('lampsTotal', fromJS(lampsTotal));
    }

    case GROUP_LAMPS_FETCH_SUCCESS: {
      return state
        .set('lampsList', fromJS(lampsData));
    }

    case SINGLE_LAMP_FETCH_SUCCESS: {
      return state
        .set('lampsList', fromJS(action.lampData));
    }

    default:
      return state;
  }
};
