import { fromJS } from 'immutable';
import { GET_REGIMES } from '../constants/actions';

function defaultState() {
  return fromJS({
    regimesList: [],
    regimesCount: 0,
  });
}

export default (state = defaultState(), action) => {
  const { regimesData, count } = action;
  switch (action.type) {

    case GET_REGIMES: {
      return state
        .set('regimesList', fromJS(regimesData))
        .set('regimesCount', fromJS(count));
    }

    default:
      return state;
  }
};
