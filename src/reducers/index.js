import { reducer as form } from 'redux-form/immutable';
import { combineReducers } from 'redux-immutablejs';
import { fromJS } from 'immutable';
import auth from './auth';
import user from './user';
import maps from './maps';
import lamps from './lamps';
import regimes from './regimes';
import notifications from './notifications';
import groups from './groups';
import zones from './zones';

const rootReducer = combineReducers(fromJS({
  auth,
  form,
  user,
  maps,
  notifications,
  lamps,
  regimes,
  groups,
  zones,
}));

export default rootReducer;
