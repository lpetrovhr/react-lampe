import { fromJS } from 'immutable';
import { QUERY_MAP } from '../constants/actions';

function defaultMapState() {
  return fromJS({
    data: [],
    mapInfo: {
      zoom: 17,
    },
  });
}

export default (state = defaultMapState(), action) => {
  const { markersData } = action;
  switch (action.type) {
    case QUERY_MAP: {
      return state
        .set('data', fromJS(markersData));
    }

    default:
      return state;
  }
};
