import { fromJS } from 'immutable';
import { GROUPS_FETCH_SUCCESS, GROUP_REGIME_CHANGE_SUCESS,
  GROUP_REGIME_CHANGE_FAIL } from '../constants/actions';

function defaultState() {
  return fromJS({
    error: null,
    groupsList: [],
    count: 0,
  });
}

export default (state = defaultState(), action) => {
  const { groupsData, count } = action;
  switch (action.type) {
    case GROUPS_FETCH_SUCCESS: {
      return state
        .set('groupsList', fromJS(groupsData))
        .set('count', fromJS(count));
    }

    case GROUP_REGIME_CHANGE_SUCESS: {
      return state
        .merge(fromJS({
          error: null,
        }));
    }

    case GROUP_REGIME_CHANGE_FAIL: {
      return state.merge(fromJS({ error: action.error }));
    }

    default:
      return state;
  }
};
